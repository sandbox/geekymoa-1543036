<?php

/**
 * Include template overrides
 */
$path = drupal_get_path('theme', 'theme_arnold');
include_once './' .$path . '/functions/css.php';
include_once './' .$path . '/functions/js.php';

//load in the goodies
if (theme_get_setting('theme_arnold_goodies_login')) {
 include_once './' .$path . '/goodies/login.inc';
}

/* ------- HTML5 IMPROVEMENTS ---------- */

///**
// * Change the default meta content-type tag to the shorter HTML5 version.
// */
function theme_arnold_html_head_alter(&$head_elements) {
  if (!module_exists('html5_tools')) {
    $head_elements['system_meta_content_type']['#attributes'] = array(
      'charset' => 'utf-8',
    );
  }
}

/**
 * Change the search form to use the HTML5 "search" input attribute.
 */
function theme_arnold_preprocess_search_block_form(&$vars) {
  if (!module_exists('html5_tools')) {
    $vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
  }
}

/* ----------- PANELS IMPROVEMENTS ------------ */

/**
 * Modify the Panels default output to remove the panel separator.
 */
function theme_arnold_panels_default_style_render_region($vars) {
  $output = '';
  $output .= implode($vars['panes']);
  return $output;
}

/* ---------- FORM IMPROVEMENTS ----------- */
/* removes the <div> wrapper inside the form */
function theme_arnold_form($variables) {
  $element = $variables['element'];
  if (isset($element['#action'])) {
    $element['#attributes']['action'] = drupal_strip_dangerous_protocols($element['#action']);
  }
  element_set_attributes($element, array('method', 'id'));
  if (empty($element['#attributes']['accept-charset'])) {
    $element['#attributes']['accept-charset'] = "UTF-8";
  }
	return '<form' . drupal_attributes($element['#attributes']) . ' role="form">' . $element['#children'] . '</form>';
}


/* --------- THEME SETTINGS --------- */

/*
  all the preprocess magic
*/
function theme_arnold_preprocess(&$vars, $hook) {
  global $theme;
  $path = drupal_get_path('theme', $theme);

  $theme_path = drupal_get_path('theme', 'theme_arnold');


  /*
    Go through all the hooks of drupal and give em epic love
  */

  if ( $hook == "html" ) {
    // =======================================| HTML |========================================

    // let's make it a tiny bit more readable in the html.tpl.php
    // gets processed in theme_arnold_process_html

    $vars['html_attributes_array'] = isset($vars['html_attributes_array']) ? $vars['html_attributes_array'] : array();

    $vars['html_attributes_array']['lang'] = $vars['language']->language;
    $vars['html_attributes_array']['dir'] = $vars['language']->dir;

    if (theme_get_setting('modernizr')) {
      drupal_add_js(libraries_get_path('modernizr') . '/modernizr.min.js', array('group' => JS_LIBRARY));
    }

    //$vars['appletouchicon'] = $appletouchicon;

    //-----<body> CSS CLASSES  -----------------------------------------------------------------------------------------------
    //Remove & add classes body

    if (theme_get_setting('theme_arnold_classes_body_html')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('html')));
    }

    if (theme_get_setting('theme_arnold_classes_body_front')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('not-front')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('front')));
    }

    if (theme_get_setting('theme_arnold_classes_body_loggedin')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('logged-in')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('not-logged-in')));
    }

    if (theme_get_setting('theme_arnold_classes_body_layout')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('two-sidebars')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('one-sidebar sidebar-first')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('one-sidebar sidebar-second')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('no-sidebars')));
    }

    if (theme_get_setting('theme_arnold_classes_body_toolbar')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('toolbar')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('toolbar-drawer')));
    }

    if (theme_get_setting('theme_arnold_classes_body_pagenode')) {
      $vars['classes_array'] = preg_grep('/^page-node/', $vars['classes_array'], PREG_GREP_INVERT);
    }

    if (theme_get_setting('theme_arnold_classes_body_nodetype')) {
      $vars['classes_array'] = preg_grep('/^node-type/', $vars['classes_array'], PREG_GREP_INVERT);
    }

    if (theme_get_setting('theme_arnold_classes_body_path')) {
      $path_all = drupal_get_path_alias($_GET['q']);
      $vars['classes_array'][] = drupal_html_class('path-' . $path_all);
    }

    if (theme_get_setting('theme_arnold_classes_body_path_first')) {
      $path = explode('/', $_SERVER['REQUEST_URI']);
      if($path['1']){
        $vars['classes_array'][] = drupal_html_class('pathone-' . $path['1']);
      }
    }

    if(isset($headers['status']) AND theme_get_setting('theme_arnold_classes_body_status') ){
      $vars['classes_array'][] = "status-". $headers['status'];
    }

    //freeform css class killing
    $remove_class_body = explode(", ", theme_get_setting('theme_arnold_classes_body_freeform'));
    $vars['classes_array'] = array_values(array_diff($vars['classes_array'],$remove_class_body));

  }elseif ( $hook == "page" ) {
    // =======================================| PAGE |========================================

    //Test for expected modules - we really love blockify
    //TODO: should this be an option to remove annoying options?

    if (isset($headers['status'])) {
      if($headers['status'] == '404 Not Found'){
        $vars['theme_hook_suggestions'][] = 'page__404';
      }

    }

    // Remove the block template wrapper from the main content block.
    if (theme_get_setting('theme_arnold_content_block_wrapper') AND
      !empty($vars['page']['content']['system_main']) AND
    $vars['page']['content']['system_main']['#theme_wrappers'] AND
    is_array($vars['page']['content']['system_main']['#theme_wrappers'])
    ) {
      $vars['page']['content']['system_main']['#theme_wrappers'] = array_diff($vars['page']['content']['system_main']['#theme_wrappers'], array('block'));
    }

    /*-
      USER ACCOUNT
      Removes the tabs from user  login, register & password
      fixes the titles to so no more "user account" all over
    */
    switch (current_path()) {
      case 'user':
        $vars['title'] = t('Login');
        unset( $vars['tabs'] );
        break;
      case 'user/register':
        $vars['title'] = t('New account');
        unset( $vars['tabs'] );
        break;
      case 'user/password':
        $vars['title'] = t('I forgot my password');
        unset( $vars['tabs'] );
        break;

      default:
        # code...
        break;
    }

  }elseif ( $hook == "block" ) {

    // =======================================| block |========================================
    //block-subject should be called title so it actually makes sence...
    //  $vars['title'] = $block->subject;
    $vars['id_block'] = "";
    if (theme_get_setting('theme_arnold_classes_block')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('block')));
    }

    if (theme_get_setting('theme_arnold_classes_block')) {
      $vars['classes_array'] = preg_grep('/^block-/', $vars['classes_array'], PREG_GREP_INVERT);
    }

    if (theme_get_setting('theme_arnold_classes_block_contextual')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('contextual-links-region')));
    }

    if (!theme_get_setting('theme_arnold_classes_block_id')) {
      $vars['id_block'] = ' id="' . $vars['block_html_id'] . '"';
    }

    if (theme_get_setting('theme_arnold_classes_block_id_as_class')) {
      $vars['classes_array'][] = $vars['block_html_id'];
    }

    //freeform css class killing
    $remove_class_block = explode(", ", theme_get_setting('theme_arnold_classes_block_freeform'));
    $vars['classes_array'] = array_values(array_diff($vars['classes_array'],$remove_class_block));


    //adds title class to the block ... OMG!
    $vars['title_attributes_array']['class'][] = 'title';
    $vars['content_attributes_array']['class'][] = 'block-content';

    //add a theme suggestion to block--menu.tpl so we dont have create a ton of blocks with <nav>
    if(
      ($vars['elements']['#block']->module == "system" AND $vars['elements']['#block']->delta == "navigation") OR
      ($vars['elements']['#block']->module == "system" AND $vars['elements']['#block']->delta == "main-menu") OR
      ($vars['elements']['#block']->module == "system" AND $vars['elements']['#block']->delta == "user-menu") OR
      ($vars['elements']['#block']->module == "admin" AND $vars['elements']['#block']->delta == "menu") OR
       $vars['elements']['#block']->module == "menu_block"
    ){
      $vars['theme_hook_suggestions'][] = 'block__menu';
    }

  }elseif ( $hook == "node" ) {
    // =======================================| NODE |========================================


    if (theme_get_setting('theme_arnold_classes_node')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('node')));
    }

    if (theme_get_setting('theme_arnold_classes_node_state')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('node-sticky')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('node-unpublished')));
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('node-promoted')));
    }
    /*
      change node-xxx to a more generalised name so we can use the same class other places
      fx in the comments
    */

    if (theme_get_setting('theme_arnold_classes_state')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('node-sticky','node-unpublished', 'node-promoted')));
      if($vars['promote']){
        $vars['classes_array'][] = 'promote';
      }
      if($vars['sticky']){
        $vars['classes_array'][] = 'sticky';
      }
      if($vars['status'] =="0"){
        $vars['classes_array'][] = 'unpublished';
      }
    }
    if (isset($vars['preview'])) {
      $vars['classes_array'][] = 'node-preview';
    }

    //freeform css class killing
    $remove_class_node = explode(", ", theme_get_setting('theme_arnold_classes_node_freeform'));
    $vars['classes_array'] = array_values(array_diff($vars['classes_array'],$remove_class_node));

    // css id for the node
    if (theme_get_setting('theme_arnold_classes_node_id')) {
      $vars['id_node'] =  'node-'. $vars['nid'];
    }

    /*
    remove class from the ul that holds the links
    <ul class="inline links">
    this is generated in the node_build_content() function in the node.module
    */
    if (theme_get_setting('theme_arnold_classes_node_links_inline') AND isset($vars['content']['links']['#attributes']['class'])) {
      $vars['content']['links']['#attributes']['class'] = array_values(array_diff($vars['content']['links']['#attributes']['class'],array('inline')));
    }

    if (theme_get_setting('theme_arnold_classes_node_links_links') AND (isset($vars['content']['links']['#attributes']['class']))) {
      $vars['content']['links']['#attributes']['class'] = array_values(array_diff($vars['content']['links']['#attributes']['class'],array('links')));
    }
    // TODO: add a field to push in whatever class names we want to
    // $vars['content']['links']['#attributes']['class'][] = "hardrock hallelulia";

    //  remove the class attribute it its empty
    if(isset($vars['content']['links']['#attributes']['class'])){
      if(isset($vars['content']['links']['#attributes']['class']) && !$vars['content']['links']['#attributes']['class']){
        unset($vars['content']['links']['#attributes']['class']);
      }
    }

  }elseif ( $hook == "comment" ) {
    // =======================================| COMMENT |========================================
    if ($vars['elements']['#comment']->new){
      $vars['classes_array'][] = ' new';
    }

    if ($vars['status'] == "comment-unpublished"){
       $vars['classes_array'][] = ' unpublished';
    }

    //remove inline class from the ul links
    if (theme_get_setting('theme_arnold_classes_node_links_inline')) {
      $vars['content']['links']['#attributes']['class'] = array_values(array_diff($vars['content']['links']['#attributes']['class'],array('inline')));
    }

  }elseif ( $hook == "field" ) {
    // =======================================| FIELD |========================================
    if (theme_get_setting('theme_arnold_classes_field_field')) {
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('field')));
    }



    //freeform css class killing
    $remove_class_field = explode(", ", theme_get_setting('theme_arnold_classes_field_freeform'));
    $vars['classes_array'] = array_values(array_diff($vars['classes_array'],$remove_class_field));

    //kill the field-name-xxxx class
    if (theme_get_setting('theme_arnold_classes_field_name')) {
      $vars['classes_array'] = preg_grep('/^field-name-/', $vars['classes_array'], PREG_GREP_INVERT);
    }
    //kill the field-type-xxxx class
    if (theme_get_setting('theme_arnold_classes_field_type')) {
      $vars['classes_array'] = preg_grep('/^field-type-/', $vars['classes_array'], PREG_GREP_INVERT);
    }

    //label
    if (theme_get_setting('theme_arnold_classes_field_label')) {
      $vars['classes_array'] = preg_grep('/^field-label-/', $vars['classes_array'], PREG_GREP_INVERT);
      $vars['classes_array'] = array_values(array_diff($vars['classes_array'],array('clearfix')));
    }

   // $vars['theme_hook_suggestions'][] = 'node__' . $vars['type'] . '__' . $vars['view_mode'];

  }elseif ( $hook == "maintenance_page" ) {
    // =======================================| maintenance page |========================================

    $vars['path'] = $path;
   // $vars['appletouchicon'] = $appletouchicon;
   //  $vars['selectivizr'] = $selectivizr;
    $vars['theme_hook_suggestions'][] = 'static__maintenance';

  }
}

function theme_arnold_preprocess_file_entity(&$variables){
  /*
   * We have striped the unnecessary divs surrounding an image tag and because 
   * of this we add the classes, id's and attributes directly to the img tag here.
   */
    
  // Add attributes to image tag
  $variables['content']['file']['#attributes'] =  array_merge($variables['attributes_array']);
  
  // Add classes and id to img tag.
  $variables['content']['file']['#attributes']['id'] = 'file-' . $variables['file']->fid;
  $variables['content']['file']['#attributes']['class'] = $variables['classes_array'];
 
  $variables['content_prefix'] = ( isset($variables['content_prefix']) ) ? $variables['content_prefix']: '' ;
  $variables['content_suffix'] = ( isset($variables['content_suffix']) ) ? $variables['content_suffix']: '' ;
  
}