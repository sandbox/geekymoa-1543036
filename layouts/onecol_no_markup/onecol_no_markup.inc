<?php
/**
 * @file
 * Definition of Lucid one col layout.
 */

$plugin = array(
  'title' => t('1 column (No markup)'),
  'theme' => 'onecol_no_markup',
  'icon' => 'onecol_no_markup.png',
  'category' => 'Theme Arnold',
  'regions' => array(
    'main' => t('Main'),
  ),
);
