<?php

/*
Nukes the js
*/
function theme_arnold_js_alter(&$js) {

  //freeform css class killing :)
  $js_kill_list = explode("\n", theme_get_setting('theme_arnold_js_freeform'));

  //grap the css and run through em
  if(theme_get_setting('theme_arnold_js_freeform')){
    foreach ($js as $file => $value) {
      //grap the kill list and do that on each file
      foreach ($js_kill_list as $key => $jsfilemustdie) {
        if (strpos($file, $jsfilemustdie) !== FALSE) {
         unset($js[$file]);
        }
      }
    }
  }

  //http://www.metaltoad.com/blog/mobile-drupal-optimization-results
  if(theme_get_setting('theme_arnold_js_jquerycdn')){
    if (isset($js['misc/jquery.js'])) {
      $js['misc/jquery.js']['data'] ='//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js';
      $js['misc/jquery.js']['type'] = 'external';
      $js['misc/jquery.js']['weight'] = -100;
    }
  }


  if(theme_get_setting('theme_arnold_js_onefile')){
    uasort($js, 'drupal_sort_css_js');
    $i = 0;
    foreach ($js as $name => $script) {
      $js[$name]['weight'] = $i++;
      $js[$name]['group'] = JS_DEFAULT;
      $js[$name]['every_page'] = FALSE;
    }
  }

}




