<?php

$plugin = array(
  'title' => t('Theme Arnold Slideshow UNFINISHED!'),
  'description' => t('Display the content of this pane as a slideshow.'),
  'render region' => 'theme_arnold_slideshow_render_region',
  'settings form' => 'theme_arnold_slideshow_settings_form',
);

/**
 * Theme function for the region style.
 */
function theme_theme_arnold_slideshow_render_region($vars) {
  $panes = $vars['panes'];
  $settings = $vars['settings'];
  $region_id = $vars['region_id'];

  $output = '<ul id="region-' . $region_id . '" class="' . $settings['wrapperClass'] . '">';
  foreach ($panes as $pane_id => $pane_output) {
    $output .= '<li class="theme-arnold-slideshow-item">' . $pane_output . '</li>';
  }

  $output .= '</ul>';
  drupal_add_js(libraries_get_path('flexslider') . '/jquery.flexslider.js');

  drupal_add_js(
    array(
      'theme_arnold' => array(
        'slideshow' => array(
          'region-' . $region_id => $settings
        )
      )
    ), 'setting');
    drupal_add_js(drupal_get_path('theme', 'theme_arnold') . '/styles/slideshow/slideshow.js');
  return $output;
}

/**
 * Settings form for the slideshow.
 */
function theme_arnold_slideshow_settings_form($settings, $display, $pid, $type, $form_state) {

  $defaults = array(
    'controlNav' => FALSE,
    'directionNav' => FALSE,
    'slideshowSpeed' => '7000',
    'animationSpeed' => '600',
    'wrapperClass' => 'theme-arnold-slideshow',
  );

  $form = array();
  $form['controlNav'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use a pager'),
    '#default_value' => isset($settings['controlNav']) ? $settings['controlNav'] : $defaults['controlNav'],
  );
  $form['directionNav'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use prev/next buttons'),
    '#default_value' => isset($settings['directionNav']) ? $settings['directionNav'] : $defaults['directionNav'],
    '#description' => t('Display next and previous buttons'),
  );
  $form['slideshowSpeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Slideshow Speed'),
    '#default_value' => isset($settings['slideshowSpeed']) ? $settings['slideshowSpeed'] : $defaults['slideshowSpeed'],
    '#description' => t('The interval in milliseconds. Set to 0 for no interval.'),
  );
  $form['animationSpeed'] = array(
    '#type' => 'textfield',
    '#title' => t('Animation Speed'),
    '#default_value' => isset($settings['animationSpeed']) ? $settings['animationSpeed'] : $defaults['animationSpeed'],
    '#description' => t('The interval in milliseconds. Set to 0 for immediate.'),
  );
  $form['wrapperClass'] = array(
    '#type' => 'textfield',
    '#title' => t('Wrapper Class'),
    '#default_value' => isset($settings['wrapperClass']) ? $settings['wrapperClass'] : $defaults['wrapperClass'],
    '#description' => t('Class used on wrapper element'),
    '#required' => TRUE,
  );
  return $form;
}