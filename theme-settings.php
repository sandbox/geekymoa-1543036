<?php

function theme_arnold_form_system_theme_settings_alter(&$form, $form_state) {

  $form['theme_arnold_info'] = array(
    '#prefix' => '<h3>Theme Arnold settings</h3> ',
    '#weight'=> -25
  );

  /* javascript */
  $form['js'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('JavaScript settings'),
    '#description'  => t('Settings for javascript'),
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
    '#weight'=> -19
  );

  $form['js']['theme_arnold_script_place_footer'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Put Javascript down at the bottom of the page.') ,
    '#default_value'  => theme_get_setting('theme_arnold_script_place_footer')
  );

  $form['js']['modernizr'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Add modernizr') ,
    '#description'   => t('Requires that modernizr exists in [libraries folder]/modernizr/modernizr.min.js'),
    '#default_value'  => theme_get_setting('modernizr')
  );

  $form['js']['theme_arnold_js_onefile'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Compress all javascript files into one file') ,
    '#description'   => t('This will compress all your js files into one file, for fewer http request '),
    '#default_value' => theme_get_setting('theme_arnold_js_onefile')
  );

  /* css files */
  $form['css'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('CSS Files'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#description'   => t('Control the css load.'),
    '#weight'=> -15
  );

  /* ----------------------------- CSS FILES  NUKE ----------------------------- */
  //Get all the posible css files that drupal could spit out
  //generate the $css_files_from_modules
  $result = db_query("SELECT * FROM {system} WHERE type = 'module' AND status = 1");
  foreach ($result as $module) {
    $module_path = pathinfo($module->filename, PATHINFO_DIRNAME);
    $css_files = file_scan_directory($module_path, '/.*\.css$/');
    foreach((array)$css_files as $key => $file) {
      $css_files_from_modules[] = $module_path . "/" . $file->filename ;
    }
  }
  //let sort em
  asort($css_files_from_modules);

  $form['css']['nuke'] = array(
    '#type'         => 'fieldset',
    '#title'        => t('Remove CSS Files'),
    '#collapsible'  => TRUE,
    '#collapsed'    => FALSE,
  );

  $form['css']['nuke']['theme_arnold_css_nuke_theme'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove .theme.css'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_theme')
   );
  $form['css']['nuke']['theme_arnold_css_nuke_admin'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove .admin.css'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_admin')
   );
  $form['css']['nuke']['theme_arnold_css_nuke_module_contrib'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove .css from contrib modules (sites/all/modules/xxx etc)'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_module_contrib')
   );
  $form['css']['nuke']['theme_arnold_css_nuke_module_all'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove all css from core Modules'),
     '#description'   => t('keeps the base.css, contextual, overlay, system & toolbar'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_module_all')
   );
  $form['css']['nuke']['theme_arnold_css_nuke_systemtoolbar'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove toolbar css'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_systemtoolbar')
   );
  $form['css']['nuke']['theme_arnold_css_nuke_system_message'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove system.messages.css'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_system_message')
   );
  $form['css']['nuke']['theme_arnold_css_nuke_system_menus'] = array(
     '#type'          => 'checkbox',
     '#title'         => t('Remove system.menus.css'),
     '#default_value' => theme_get_setting('theme_arnold_css_nuke_system_menus')
   );
   $form['css']['nuke']['theme_arnold_css_nuke_system_theme'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Remove system.theme.css'),
      '#default_value' => theme_get_setting('theme_arnold_css_nuke_system_theme')
    );



  //remove the css thats already remove by BAT
  foreach ($css_files_from_modules as $file => $value) {

    switch (theme_get_setting('theme_arnold_nuke_css')) {
      //full
      case 'theme_arnold_css_nuke_theme_full':
        if (strpos($value, 'theme.css') !== FALSE) {
          unset($css_files_from_modules[$file]);
        }
      break;
      //theme.css
      case 'theme_arnold_css_nuke_theme':
        if (strpos($value, 'theme.css') !== FALSE) {
          unset($css_files_from_modules[$file]);
        }
        break;

      case 'theme_arnold_css_nuke_admin':
        if (strpos($value, 'admin.css') !== FALSE) {
          unset($css_files_from_modules[$file]);
        }
        break;

      case 'theme_arnold_css_nuke_theme_admin':
        if (strpos($value, 'theme.css') !== FALSE) {
          unset($css_files_from_modules[$file]);
        }
        if (strpos($value, 'admin.css') !== FALSE) {
          unset($css_files_from_modules[$file]);
        }
        break;

      case 'theme_arnold_css_nuke_module':
        if (strpos($value, 'module') !== FALSE) {
          unset($css_files_from_modules[$file]);
        }

        break;

      case 'theme_arnold_css_nuke_epic':
          unset($css_files_from_modules);
        break;

      default:
        # code...
        break;
    }
  }

  //now that we have cleared up from the BAT its time for some freeform removing :)

  /* ----------------------------- STYLE STRIPPING ----------------------------- */
  $form['css']['nuke']['stylestripper'] = array(
     '#type'          => 'fieldset',
     '#title'         => t('CSS File Stripping ') . sizeof($css_files_from_modules). ' CSS Files',
     '#collapsible' => TRUE,
     '#collapsed' => TRUE,
   );

  $form['css']['nuke']['stylestripper']['theme_arnold_css_freeform'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Path to the CSS files you want to strip out.'),
    '#default_value' => theme_get_setting('theme_arnold_css_freeform'),
    '#description'   => t("The whole path to the file(s) that should be removed from the theme, on pr line. <br>this list doesn't account for the BAT removal, will come in a later release"),
    '#suffix'       => '<strong>CSS file paths, based on the modules loaded in you Drupal setup</strong><br>'.  implode('<br> ', $css_files_from_modules )
  );

  //list of css files with links
  global $base_url;


  $form['classes'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('Classes & Markup'),
      '#description'   => t('Settings to change classes & markup that Drupal drags around'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#weight'=> -11
  );

  //---------------- BODY
  $form['classes']['body'] = array(
      '#type'          => 'fieldset',
      '#title'         => t('body classes'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#description'   => t('Modifies the css in the body tag <b> &lt;body class="html logged-in front sidebar toolbar page-node"&gt; </b> html.tpl.php'),
  );

  $form['classes']['body']['theme_arnold_classes_body_html'] = array(
      '#type'          => 'checkbox',
      '#title'         => t('Remove .html'),
      '#default_value' => theme_get_setting('theme_arnold_classes_body_html')
  );

  $form['classes']['body']['theme_arnold_classes_body_loggedin'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .logged-in if a user is logged in'),
    '#default_value' => theme_get_setting('theme_arnold_classes_body_loggedin')
  );

  $form['classes']['body']['theme_arnold_classes_body_front'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove Frontpage Status (.front / .not-front)'),
    '#default_value' => theme_get_setting('theme_arnold_classes_body_front')
  );

  $form['classes']['body']['theme_arnold_classes_body_layout'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove the layout classes (.one-sidebar | .sidebar-first | .sidebar-last)'),
    '#default_value' => theme_get_setting('theme_arnold_classes_body_layout')
  );

  $form['classes']['body']['theme_arnold_classes_body_toolbar'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove Toolbar (.toolbar & .toolbar-drawer )'),
    '#default_value' => theme_get_setting('theme_arnold_classes_body_toolbar')
  );

  $form['classes']['body']['theme_arnold_classes_body_pagenode'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .page-node-[...]'),
    '#default_value' => theme_get_setting('theme_arnold_classes_body_pagenode')
  );

  $form['classes']['body']['theme_arnold_classes_body_nodetype'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .node-type'),
    '#default_value' => theme_get_setting('theme_arnold_classes_body_nodetype')
  );

   $form['classes']['menu'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Menus'),
   '#description'   => t('Modifies the stuff that drupal wraps around the menu ul & li tags'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

   /* form classes */
  $form['classes']['menu']['theme_arnold_classes_menu_items_mlid'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove the menu-mlid-[mid] class'),
    '#default_value' => theme_get_setting('theme_arnold_classes_menu_items_mlid')
  );


  $form['classes']['menu']['theme_arnold_classes_menu_wrapper'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove the .menu-wrapper class on the &lt;ul class="menu-wrapper" &gt; menustuff &lt;/ul&gt; '),
    '#default_value' => theme_get_setting('theme_arnold_classes_menu_wrapper')
  );


  $form['classes']['menu']['theme_arnold_classes_menu_items_firstlast'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .first & .last class from the li '),
    '#default_value' => theme_get_setting('theme_arnold_classes_menu_items_firstlast')
  );

  $form['classes']['menu']['theme_arnold_classes_menu_items_active'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .active & .active-trail from the li '),
    '#default_value' => theme_get_setting('theme_arnold_classes_menu_items_active')
  );

  $form['classes']['menu']['theme_arnold_classes_menu_collapsed'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .collapsed, .expandable & .expanded from the li '),
    '#default_value' => theme_get_setting('theme_arnold_classes_menu_collapsed')
  );

  $form['classes']['menu']['theme_arnold_classes_menu_leaf'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .leaf from the li '),
    '#default_value' => theme_get_setting('theme_arnold_classes_menu_leaf')
  );

  //---------------- Views
  $form['classes']['view'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('view classes '),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['classes']['view']['theme_arnold_classes_view'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .view'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view')
  );
  $form['classes']['view']['theme_arnold_classes_view_name'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .$viewname'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view_name')
  );
  $form['classes']['view']['theme_arnold_classes_view_view_id'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .view-id-$viewname & .view-display-id-$viewname'),
   '#description'   => t('You dont wanna do this is your wanna use the ajax pagination - just saying'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view_view_id')
  );

  $form['classes']['view']['theme_arnold_classes_view_row'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .view-row'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view_row')
  );
  $form['classes']['view']['theme_arnold_classes_view_row_count'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .view-row-$count'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view_row_count')
  );
  $form['classes']['view']['theme_arnold_classes_view_row_first_last'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Remove .view-row-first & .view-row-last'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view_row_first_last')
  );
  $form['classes']['view']['theme_arnold_classes_view_row_rename'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Rename .view-row-$count,  .view-row-first & .view-row-last to : count-$count, .first & .last'),
    '#description'   => t('To make sure that we use .first & .last classes all over the site'),
    '#default_value' => theme_get_setting('theme_arnold_classes_view_row_rename')
  );

  /* misc goodie bag */
  $form['misc'] = array(
    '#type'          => 'fieldset',
    '#title'         => t('Theme Arnold misc goodie bag'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#weight'=> -10
  );

  $form['misc']['theme_arnold_goodies_login'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('<del>Alternative</del> Better! user login block'),
    '#description'   => t('Changes the design of the login block: puts register link first, then the username / password, then forgot password & last submit button'),
    '#default_value' => theme_get_setting('theme_arnold_goodies_login'),
  );

}